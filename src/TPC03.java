import kr.bit.Book;
import kr.bit.PersonVO;

public class TPC03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//관계를 이해하기 PD vs UDDP
		
		//정수 1개를 저장하기 위한 변수를 선언하시오
		int a;
		a = 10;
		
		//책 1권을 저장하기위한 변수를 선언하시오
		//Book b; -> 책이라는 자료형을 사용하기 위해서는 만들어야 한다.

		Book b; //경로를 설정해주어서 class를 호출해줌
		//메모리 어딘가에 책이라는 객체가 생성
		//선언된 변수는 책의 구성요소 번지를 찾아가게 해준다
		
		
		//b라는 변수가 메모리에 선언됨 -> 책을 b에 담아야함
		//-> 실체를 만드는 과정: 객체 생성
		b = new Book(); //객체를 생성하여 생성된 객체에 번지를 넣어주게 되면 b가 책 1권이 됨
	
		b.title = "자바"; //b가 가르키는 곳의 제목
		b.price = 15000;
		b.company = "한빛미디어";
		b.page = 700;
		
		//책한권의 데이터를 만들어서 값을 저장하고 저장되어있는 데이터를 출력
		System.out.print(b.title+"\t");
		System.out.print(b.price+"\t");
		System.out.print(b.company+"\t");
		System.out.println(b.page);
		
		
		PersonVO p;
		
		p = new PersonVO();
		
		//접근하기
		p.name = "오매일";
		p.age = 40;
		p.weight = 68.4f;
		p.height = 175.7f;
		
		System.out.print(p.name+"\t");
		System.out.print(p.age+"\t");
		System.out.print(p.weight+"\t");
		System.out.println(p.height);
		
		

	}

}
