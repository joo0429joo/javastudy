
public class TPC02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//프로그래밍의 3대요소: 변수, 자료형(data type), 할당(연산자 =)
		//1+1 = 2 -> 메모리에서 계산 -> 기억공간 필요(변수)
		// 1. 크기, 2. data 종류 고려하기 -> 자료형(data type)

		int a, b, c; //변수 선언 -> 4byte 크기의 기억공간이 생성됨
		
		a = 1; //할당
		b = 1;
		
		c = a + b;
		
		System.out.println(c);
		
		float f; //실수형
		f = 34.5f;
		
		System.out.println(f);
		
		char d;
		d = 'A'; //문자
		
		System.out.println(d);
		
		boolean g;
		g = true;
		
		System.out.println(g); //기본 자료형
		
		
		//제공하지 않는 자료형을 쓰고 싶을 경우
		//Book bk; 사용자가 설계하여 class를 만들어서 사용 -> 객체 지향
		
		
		
		
		
		
		
		
		
	}

}
