package kr.bit;

//회원
public class PersonVO {//배열로는 동일한 데이터를 넣어야하기 때문에 class로 생성하는 것이 좋다.
	
	public String name;
	public int age;
	public float weight;
	public float height;

}//클래스 안에 상태정보(= 속성, 멤버변수) 생성 -> 실제로 메모리에 만들어짐
//만들어진 실물: instance
