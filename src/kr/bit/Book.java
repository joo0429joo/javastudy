package kr.bit; //package 선언문

//현실 세계에 있는 책의 구조를 java  프로그램으로 설계
//책(객체)/ 제목, 가격, 출판사, 페이지 수 .... (상태 정보+행위정보)
public class Book { //사용자 정의 자료 형 -> class로 만듬
	
	//기억공간 여러개를 하나의 구조로 붙혀놓음 -> 객체 -> 설계 -> class
	
	public String title;
	public int price;
	public String company;
	public int page;
	//설계도 -> 모형 -> 실체
	//설계하고 생성하여 메모리에 실제 공간을 만듬 -> 객체 생성
	
	
}
